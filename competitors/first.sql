﻿-- create table scrapes.ext_competitor_vehicle_data(
-- vin citext, 
-- gm_json json, 
-- chrome_json json)
-- 
-- insert into scrapes.ext_competitor_vehicle_data (vin, chrome_json)
-- 

/*

drop table if exists base_inventory;
create temp table base_inventory as
with paid_options as (

select distinct vin, concat(gm_option->>'OptionCode', ' - ', gm_option->>'OptionDescription') as code
from scrapes.ext_competitor_vehicle_data a
left join json_array_elements(gm_json) as gm_data on true
left join json_array_elements(gm_data->'Option') as gm_option on true
left join json_array_elements(gm_option->'Price') as gm_option_price on true
where gm_json::citext <> 'null'
and gm_option_price is not null
and gm_option_price->>'Value' <> '0.0'
) 
 select vin, model_year, body_type, make, model, chrome_model_code, trim_level, gm_model_code,
 array(select code from paid_options zz where a.vin = zz.vin) as paid_options
 from (
select vin, chrome_json->'vinDescription'->'attributes'->>'modelYear' as model_year,
 chrome_json->'vinDescription'->'attributes'->>'bodyType' as body_type,

 chrome_json->'vinDescription'->'attributes'->>'division' as make,
 chrome_json->'vinDescription'->'attributes'->>'modelName' as model,
 chrome_style->'attributes'->>'mfrModelCode' as chrome_model_code,
 chrome_style->'attributes'->>'trim' as trim_level, 
 gm_data->>'Model' as gm_model_code
 from scrapes.ext_competitor_vehicle_data a
left join json_array_elements(gm_json) as gm_data on true
left join json_array_elements(chrome_json->'style') as chrome_style on true
where gm_json::citext <> 'null'
and chrome_json is not null
)  a
where chrome_model_code = gm_model_code;
*/
-- END TEMP TABLE
-- drop table if exists blazer;
-- create temp table blazer as 
-- with paid_options as (
-- select distinct vin,  concat(gm_option->>'OptionCode', ' - ', gm_option->>'OptionDescription') as code
-- from scrapes.ext_competitor_vehicle_data a
-- left join json_array_elements(gm_json) as gm_data on true
-- left join json_array_elements(gm_data->'Option') as gm_option on true
-- left join json_array_elements(gm_option->'Price') as gm_option_price on true
-- where gm_json::citext <> 'null'
-- and gm_option_price is not null
-- and gm_option_price->>'Value' <> '0.0'
-- and chrome_json is null )
-- 
-- 
-- select  a.vin, stock_number, b.color, msrp, price, b.make, b.model, b.model_year, b.trim_level, model_code, array(select code from paid_options zz where a.vin = zz.vin) as paid_options
-- from gmgl.get_vehicle_prices() a
-- inner join nc.vehicles b on a.vin = b.vin
-- where a.model like '%blazer%';


-- select array(select unnest(:arr1) except select unnest(:arr2));


select a.vin as our_vin, b.vin as competitor_vin, a.model_year, a.make, a.model, a.trim_level, a.model_code,  a.price as rydell_best_price, b.price as competitor_web_price, a.paid_options as rydell_additional_options,
b.paid_options as competitor_additional_options, array(select unnest(a.paid_options) except select unnest(b.paid_options)) as options_we_have_they_dont,
array(select unnest(a.paid_options) intersect select unnest(b.paid_options)) as options_we_have_they_also_have,
array(select unnest(b.paid_options) except select unnest(a.paid_options)) as options_they_have_we_dont, 
vdp_url as competitor_link
from blazer A
left join (
select a.*, price, scraping_url, vdp_url
from base_inventory a
inner join scrapes.competitor_websites_data b on a.vin = b.vin and lookup_date = current_date 
) b on a.make = b.make and a.model = b.model and a.model_code = b.gm_model_code and a.trim_level = b.trim_level and a.model_year::citext = b.model_year

-- 
-- 
-- select a.*, price, scraping_url
-- from base_inventory a
-- inner join scrapes.competitor_websites_data b on a.vin = b.vin and lookup_date = current_date 
-- 
-- ;

