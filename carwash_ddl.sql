﻿-- create table  nrv.car_washes(
-- car_wash_title citext not null primary key, 
-- normal_playlist_id citext not null, 
-- maintenance_playlist_id citext not null)

-- insert into nrv.car_washes values ('Car Wash North', 'b650b4f7-1dec-4b53-bd98-447cb4940d28','5749e017-ee17-4c5b-8c78-336fc810deb0');
-- insert into nrv.car_washes values ('Car Wash South', '8ea7d682-60a0-44c8-88ee-96bc7169c53a', '5d591a38-6cfb-40cb-90b4-dde1c7889e28');

-- CREATE TABLE nrv.car_wash_statuses
-- (
--   car_wash_title citext not null,
--   status citext NOT NULL,
--   from_ts timestamp with time zone NOT NULL,
--   thru_ts timestamp with time zone NOT NULL,
--   user_key uuid NOT NULL,
--   CONSTRAINT car_wash_statuses_pk PRIMARY KEY (car_wash_title, thru_ts),
--   constraint car_wash_title_fk foreign key (car_wash_title) references nrv.car_washes,
--   CONSTRAINT car_wash_statuses_user_fk FOREIGN KEY (user_key)
--       REFERENCES nrv.users (user_key) MATCH SIMPLE
--       ON UPDATE NO ACTION ON DELETE NO ACTION
-- )

insert into nrv.car_wash_statuses values ('Car Wash North', 'up', now(), '12/31/9999','1dc9fc6a-6c05-47b3-b3e3-0c0ebc32be5a');
insert into nrv.car_wash_statuses values ('Car Wash South', 'up', now(), '12/31/9999', '1dc9fc6a-6c05-47b3-b3e3-0c0ebc32be5a');