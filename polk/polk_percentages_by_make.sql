﻿create or replace function polk.get_make_percentages() returns setof json as $body$

select json_agg(row_to_json(A))
from (
select make, round(total_registrations_by_make::numeric/total_registrations::numeric,5) as percentage
from (
  select make, sum(registrations) as total_registrations_by_make, 
  (select sum(registrations) from polk.monthly_registrations where registration_type = 'retail' and new_used = 'new') total_registrations
  from polk.monthly_registrations a
  inner join polk.vehicle_categories b on a.vehicle_id = b.vehicle_id
  where registration_type = 'retail'
  and new_used = 'new'
  group by make
) A
order by round(total_registrations_by_make::numeric/total_registrations::numeric,2) desc ) A

$body$
language sql 